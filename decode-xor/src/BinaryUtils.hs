module BinaryUtils where

asBinaryString :: (Integral a) => a -> [Char]
asBinaryString = asBinaryStringFixedLength 0

asBinaryStringFixedLength :: (Integral a) => Int -> a -> [Char]
asBinaryStringFixedLength minLength num = padZeros (minLength - (length binaryRepresentation)) ++ binaryRepresentation
    where
        padZeros count
            | count <= 0 = ""
            | otherwise = take count $ repeat '0'
        asBinaryStringRec 0   = ""
        asBinaryStringRec num = (asBinaryStringRec (num `div` 2)) ++ (if num `mod` 2 == 0 then "0" else "1")
        binaryRepresentation = asBinaryStringRec num