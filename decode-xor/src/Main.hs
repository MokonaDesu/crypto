{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import Data.Yaml
import Data.List
import System.TimeIt
import System.Environment

import ListUtils
import BinaryUtils

-- Input data configuration. Read from Data.yml in the root of the program.
data Config = Config {
    -- Library of all possible words
    words    :: [[Char]],
    -- All possible characteds that make up a word
    alphabet :: [Char],
    -- The encoded string
    encoded  :: [Char],
    -- Length of the single word in bits
    wordLength :: Int,
    -- Number of words in the sequence
    wordCount :: Int
} deriving (Show, Generic)
instance FromJSON Config

-- Found match configuration. A combination of a matching alphabet config and words.
data Match = Match {
    -- Matching words that are found
    wordsConfig :: [[Char]],
    
    -- Alphabet config that is appropartiate
    alphabetConfig :: [(Char, [Char])]
} deriving (Show)

xorCombine :: ([Char], [Char]) -> [Char]
xorCombine pair = map combineSymbol (zip (fst pair) (snd pair))
    where
        combineSymbol ('1', '0') = '1'
        combineSymbol ('0', '1') = '1'
        combineSymbol ('0', '0') = '0'
        combineSymbol ('1', '1') = '0'
        combineSymbol ( _ ,  _ ) = 'X'
        
-- Encodes a single character in accordance to the alphabet config
encodeCharacter :: [(Char, [Char])] -> Char -> [Char]
encodeCharacter letterCombination character = case find (\x -> (fst x) == character) letterCombination of
    Just combination -> snd combination
    Nothing          -> "?"

-- Encodes a single word in accordance with an alphabet config
encodeWord :: [(Char, [Char])] -> [Char] -> [Char]
encodeWord letterCombination word = concat $ map (encodeCharacter letterCombination) word 

-- Decodes a single character in accordance to the alphabet config
decodeCharacter :: [(Char, [Char])] -> [Char] -> Char
decodeCharacter letterCombination encodedCharacter = case find (\x -> (snd x) == encodedCharacter) letterCombination of
    Just combination -> fst combination
    Nothing          -> '?'

-- Decodes a single word in accorance to the alphabet config
decodeWord :: [(Char, [Char])] -> Int -> [Char] -> [Char]
decodeWord letterCombination wordLengthBits word = map (decodeCharacter letterCombination) (splitEvery wordLengthBits word)

-- Attempts to find a match for the given letter combination
getMatchFor :: Config -> [[Char]] -> [(Char, [Char])] -> Match
getMatchFor config combinedBits letterCombination = 
    let allMatchingPairs = filter isMatchingPair allPossibleUniquePairs in
        Match (nub $ concat $ map pairToList allMatchingPairs) letterCombination
    where
        wordsWithCurrentAlphabetConfig = map (encodeWord letterCombination) (Main.words config)
        allPossibleUniquePairs = uniquePairs wordsWithCurrentAlphabetConfig
        isMatchingPair pair = (xorCombine pair) `elem` combinedBits
        	       
-- Attempts to decode the given configuration of words/letters/etc
attemptDecode :: Config -> [Match]
attemptDecode config = 
    filter isValidMatch (map (getMatchFor config combinedBits) letterCombinations)
    where
        encodedString = reverse (encoded config)
        wordLengthBits = wordLength config
        wordsBits = splitEvery wordLengthBits encodedString  
        combinedBits = map xorCombine (uniquePairs wordsBits)
        mapAlphabet letters = zip letters $ map (asBinaryStringFixedLength 3) [0..]
        letterCombinations = map mapAlphabet (permutations (alphabet config))
        isValidMatch match = (length $ wordsConfig match) == (wordCount config)

-- Executes the decoding operation and pretty-prints the results
doDecode :: Maybe Config -> IO ()
doDecode inputs = do
    case inputs of
        Nothing     -> putStrLn "Input yaml is invalid. =( Better luck next time!"
        Just config -> putStrLn $ ("Valid alphabets are:\n\t" ++ (show $ length decodedResults) ++ "\n\nDecoded words are:\n\t" ++ (show $ map (decodeWord (alphabetConfig $ head decodedResults) 3) (wordsConfig $ head decodedResults))) ++ "\n"
            where
                decodedResults = attemptDecode config
                
-- Entry point of the program
main :: IO ()
main = do  
    args <- getArgs
    inputs <- decodeFile (if ("--short" `elem` args) then "DataShort.yml" else "Data.yml") :: IO (Maybe Config)
    timeIt (doDecode inputs)
