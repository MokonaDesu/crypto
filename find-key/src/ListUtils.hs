module ListUtils where
import Data.List

-- Splits the input list into several parts no larger than @ elements
splitEvery :: Int -> [a] -> [[a]]
splitEvery _  [] = []
splitEvery at xs = (fst splitted) : (splitEvery at (snd splitted))
    where
        splitted = splitAt at xs

-- Builds all unique pairs of items from the given list
uniquePairs :: Ord a => [a] -> [(a, a)]
uniquePairs xs
    | length xs < 2 = []
    | otherwise     = nub [(x, y) | x <- xs, y <- xs, x < y]
        
-- Build all unique triples of items from the given list (by transposition)
uniqueTriples :: Ord a => [a] -> [(a, a, a)]
uniqueTriples xs
    | length xs < 3 = []
    | otherwise     = nub [(x, y, z) | x <- xs, y <- xs, z <- xs, x < y && y < z]
    
-- Turns a tuple of three into a list of three
tripleToList :: (a, a, a) -> [a]
tripleToList (a, b, c) = [a, b, c]

-- Turns a tuple of two into a list of two
pairToList :: (a, a) -> [a]
pairToList (a, b) = [a, b]

-- Swaps the tuple's parts
swapTuple :: (a, b) -> (b, a)
swapTuple (a, b) = (b, a)

--Flattens the list of lists into a single list
flatten :: [[a]] -> [a]         
flatten xs = (\z n -> foldr (\x y -> foldr z y x) n xs) (:) []

--Returns true when second arg is the subset of the first
isSubsetOf :: Eq a => [a] -> [a] -> Bool
isSubsetOf set = all (`elem` set)