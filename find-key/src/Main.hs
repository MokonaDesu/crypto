import System.IO  
import Control.Monad
import MathUtils
import ListUtils
import Data.Char

prime = 263
totientOfPrime = MathUtils.totient prime
alphabet = [(ord ' ')..(ord '~')]
a = 5
b = 7
        
bytifyText :: [Char] -> [Int]
bytifyText = map ord

textifyBytes :: [Int] -> [Char]
textifyBytes = map chr

getMultipliers :: Int -> Int -> Int -> [Int]
getMultipliers d m val = [x | x <- [0..prime-1], ((val * x) `mod` d) == m]

encodeByte :: Int -> Int -> Int
encodeByte key byte = (fromInteger (((toInteger byte) ^ (toInteger key)) `mod` (toInteger prime)))

applyKey :: Int -> [Int] -> [Int]
applyKey key = map (encodeByte key)

decodeText :: [Int] -> [([Int], (Int, Int))]
decodeText bytes = (filter (\config -> isSubsetOf alphabet (fst config)) allMu4s)
    where
        possibleMuls = map (\pair -> (fst pair, (snd pair) !! 0)) $ filter (\mults -> not $ null (snd mults)) $ map (\a -> (a, (getMultipliers totientOfPrime 1 a))) [0..prime-1]
        argumentPairs = ListUtils.uniquePairs (map snd possibleMuls)
        allFunctors = map (\pair -> (((applyKey $ snd pair) . (applyKey $ fst pair)), pair)) argumentPairs
        allMu4s = map (\functorConfig -> ((fst functorConfig) bytes, snd functorConfig)) allFunctors
       
main :: IO ()
main = do
    contents <- readFile "Data.txt"
    encodedText <- return $ (applyKey b) $ (applyKey a) $ bytifyText contents
    decodingData <- return $ decodeText encodedText
    putStrLn $ "Decoded result: " ++ (textifyBytes $ fst $ (!!0) $ decodingData)
    possibleSolutions <- return $ map snd $ decodingData
    putStrLn $ "Possible solutions: " ++ (show $ possibleSolutions)
    putStrLn $ "Total of: " ++ (show $ length possibleSolutions)