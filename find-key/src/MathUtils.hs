module MathUtils where

coprime :: Int -> Int -> Bool
coprime x y = gcd x y == 1

totient :: Int -> Int
totient x = length [y | y <- [1..x], coprime y x]
