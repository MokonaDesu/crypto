import System.IO  
import Control.Monad

import Data.Char

import Data.List
import Data.List.Split

import BinaryUtils
import ListUtils

-- The encoding metadata struct
data EncodingConfig = EncodingConfig {
    -- The separator to include in between the characters
    separator :: [Char],
    
    -- The encoding targets. fst charCode is the key. snd charCode is the encodedValue
    charCodes :: [(Char, [Char])]
} deriving (Show)

prepareInput :: [Char] -> [Char]
--prepareInput text = [toLower x | x <- text, isSpace x || isLetter x]
prepareInput text = text

-- Analyzes the letter frequency composition of the text
frequencies :: [Char] -> [(Char, Int)]
frequencies text = sortOn (negate . snd) $ map (\ltr -> (ltr, (inclusions ltr text))) (nub text)
    where inclusions x = length . filter (==x)
    
-- Calculates the smallest available separator for the letters
findSeparator :: [[Char]] -> [Char]
findSeparator combinations = head $ dropWhile isInvalidSeparator (map asBinaryString [1..])
    where
        isInvalidSeparator separator = any (isInfixOf separator) combinations
        
-- Builds the compression configuration based on the frequency analysis results
buildEncodingConfig :: [(Char, Int)] -> EncodingConfig
buildEncodingConfig frequencies = EncodingConfig configSeparator encodedChars
    where
        encodedChars = zip (map fst frequencies) (map (tail . (\x -> (asBinaryString x))) [1, 3..])
        configSeparator = findSeparator $ map snd encodedChars        
        
-- Encodes the single character based on the config
lookupEncoding :: EncodingConfig -> Char -> [Char]
lookupEncoding encodingConfig char = case lookup char (charCodes encodingConfig) of
    Just charCode -> charCode
    Nothing   -> "?"

-- Decodes the bit string into a single character based on the config
lookupDecoding :: EncodingConfig -> [Char] -> Char
lookupDecoding encodingConfig charCode = case lookup charCode (map swapTuple (charCodes encodingConfig)) of
    Just char -> char
    Nothing   -> '?'
    
-- Encodes (compresses) the string
freqEncode :: [(Char, Int)] -> [Char] -> (EncodingConfig, [Char])
freqEncode frequencies text = (encodingConfig, concat $ intersperse (separator encodingConfig) $ map (lookupEncoding encodingConfig) text)
    where encodingConfig = buildEncodingConfig frequencies

-- Decodes (decompresses) the string
freqDecode :: EncodingConfig -> [Char] -> [Char]
freqDecode config text = map (lookupDecoding config) (splitOn (separator config) text)
    
main :: IO ()
main = do
    contents <- readFile "Data.txt"
    
    putStrLn "\nEncoding config: " 
    putStrLn $ show $ buildEncodingConfig $ (frequencies (prepareInput contents))
    
    putStrLn "\nEstimated initial size: "
    putStrLn $ show $ (length (prepareInput contents)) * 8

    putStrLn "\nCompressed text: "
    putStrLn $ let (config, encoded) = freqEncode (frequencies (prepareInput contents)) (prepareInput contents) in concat $ intersperse "\t" $ intersperse (separator config) $ splitOn (separator config) encoded
    
    putStrLn "\nCompressed size: "
    putStrLn $ show $ length $ snd $ freqEncode (frequencies (prepareInput contents)) (prepareInput contents)
    
    putStrLn "\nDecompressed text: "
    putStrLn $ let (config, encoded) = (freqEncode (frequencies (prepareInput contents)) (prepareInput contents)) in freqDecode config encoded
    
    putStrLn "\nEstimated bits saved: "
    putStrLn $ show $ (length (prepareInput contents)) * 8 - (length $ snd $ (freqEncode (frequencies (prepareInput contents)) (prepareInput contents)))