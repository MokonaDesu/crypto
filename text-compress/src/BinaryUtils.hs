module BinaryUtils where

-- Shows the number as a binary string of varying length
asBinaryString :: (Integral a) => a -> [Char]
asBinaryString = asBinaryStringFixedLength 0

-- Shows the number as a binary string of a fixed length
asBinaryStringFixedLength :: (Integral a) => Int -> a -> [Char]
asBinaryStringFixedLength minLength num = padZeros (minLength - (length binaryRepresentation)) ++ binaryRepresentation
    where
        padZeros count
            | count <= 0 = ""
            | otherwise = take count $ repeat '0'
        asBinaryStringRec 0   = ""
        asBinaryStringRec num = (asBinaryStringRec (num `div` 2)) ++ (if num `mod` 2 == 0 then "0" else "1")
        binaryRepresentation = asBinaryStringRec num

-- Combines two bit sequences with an XOR operation
xorCombine :: ([Char], [Char]) -> [Char]
xorCombine pair = map combineSymbol (zip (fst pair) (snd pair))
    where
        combineSymbol ('1', '0') = '1'
        combineSymbol ('0', '1') = '1'
        combineSymbol ('0', '0') = '0'
        combineSymbol ('1', '1') = '0'
        combineSymbol ( _ ,  _ ) = 'X'